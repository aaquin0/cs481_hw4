﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ListView
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public IList<Fruit> fruits { get; private set; }

        public MainPage()
        {
            InitializeComponent();

            fruits = new List<Fruit>();

            fruits.Add(new Fruit
            {
                name = "Apple",
                color = "Red",
                latin = "malus"
            });

            fruits.Add(new Fruit
            {
                name = "Banana",
                color = "Yellow",
                latin = "musa"
            });

            fruits.Add(new Fruit
            {
                name = "Blueberry",
                color = "Blue",
                latin = "vaccinium"
            });

            fruits.Add(new Fruit
            {
                name = "Cherry",
                color = "Red",
                latin = "cerasus"
            });

            fruits.Add(new Fruit
            {
                name = "Figs",
                color = "Purple",
                latin = "ficorum"
            });

            fruits.Add(new Fruit
            {
                name = "Grapes",
                color = "Purple",
                latin = "labruscum"
            });

            fruits.Add(new Fruit
            {
                name = "Lemon",
                color = "Yellow",
                latin = "citrea"
            });

            fruits.Add(new Fruit
            {
                name = "Lime",
                color = "Green",
                latin = "cinis"
            });
            fruits.Add(new Fruit
            {
                name = "Kiwifruit",
                color = "Brown",
                latin = "kiwi fructum"
            });
            fruits.Add(new Fruit
            {
                name = "Orange",
                color = "Orange",
                latin = "aurantiaco"
            });

            fruits.Add(new Fruit
            {
                name = "Pear",
                color = "Green",
                latin = "pirum"
            });

            fruits.Add(new Fruit
            {
                name = "Strawberry",
                color = "Red",
                latin = "fragum"
            });

            BindingContext = this;
        }

        void Handle_Refreshing(object sender, EventArgs e)
        {
            fruitList.IsRefreshing = false;
        }

        void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
        {
            var fruits = e.SelectedItem as Fruit;

            ContentPage page = null;

            switch (fruits.name)
            {
                case "Apple":
                    page = new Apple();
                    break;
                case "Banana":
                    page = new Banana();
                    break;
                case "Blueberry":
                    page = new Blueberry();
                    break;
                case "Cherry":
                    page = new Cherry();
                    break;
                case "Figs":
                    page = new Figs();
                    break;
                case "Grapes":
                    page = new Grapes();
                    break;
                case "Lemon":
                    page = new Lemon();
                    break;
                case "Lime":
                    page = new Lime();
                    break;
                case "Kiwifruit":
                    page = new Kiwifruit();
                    break;
                case "Orange":
                    page = new Orange();
                    break;
                case "Pear":
                    page = new Pear();
                    break;
                case "Strawberry":
                    page = new Strawberry();
                    break;
                default:
                    break;

            }

            page.BindingContext = fruits;
            Navigation.PushModalAsync(page);
        }

        async void ShowClicked(object sender, EventArgs e)
        {
            await DisplayAlert("Show", "This is the context menu test", "OK");
        }
    }

    public class Fruit
    {
        public string name { get; set; }
        public string color { get; set; }
        public string latin { get; set; }

        public override string ToString()
        {
            return name;
        }
    }
}
